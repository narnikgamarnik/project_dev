from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from datetime import datetime
import os
import time

def building_directory_path(instance, filename):
    return 'buildings/building_{0}/{1}'.format(instance.buildings.pk, filename)

def lease_files(instance, filename):
    return 'properties/property_{0}/property_lease_{1}/{2}'.format(instance.prop.pk, instance.property_lease_info.pk, filename)

def validate_file_extension(value):
  ext = os.path.splitext(value.name)[1]
  print(value, value.name, ext)
  valid_extensions = ['.pdf','.doc','.docx', '.jpeg','.jpg', '.tif']
  if not ext in valid_extensions:
    raise ValidationError(u'Please upload files in PDF or Word format only')
  return value


class Buildings(models.Model):
    building_name=models.CharField(max_length=50, null=False)
    date_added=models.DateTimeField(auto_now_add=True, null=True, blank=True)
    update_date=models.DateTimeField(auto_now=True, null=True, blank=True)
    added_by=models.ForeignKey(settings.AUTH_USER_MODEL, related_name='building_added_by', null=True, blank=True)
    address = models.CharField(max_length=50, null=True, blank=True)
    city=models.CharField(max_length=20, null=True, blank=True)
    province=models.CharField(max_length=30, null=True, blank=True)
    postal_code=models.CharField(max_length=7, null=True, blank=True)
    mls_zone=models.CharField(max_length=10, null=True, blank=True)
    developer=models.CharField(max_length=100, null=True, blank=True)
    architect=models.CharField(max_length=100, default="", null=True, blank=True)
    landscape_architect=models.CharField(max_length=100, null=True, blank=True)
    marketing_and_press=models.CharField(max_length=100, null=True, blank=True)
    urbantoronto_category=models.CharField(max_length=100, null=True, blank=True)
    status=models.CharField(max_length=30, null=True, blank=True)
    number_of_buildings=models.CharField(max_length=30, null=True, blank=True)
    building_height=models.CharField(max_length=20, null=True, blank=True)
    building_storeys=models.CharField(max_length=20, null=True, blank=True)
    number_of_units=models.CharField(max_length=20,null=True, blank=True)
    completion_date=models.DateField(null=True, blank=True, )
    building_electrical_provider=models.CharField(max_length=30,null=True)
    building_cable_provider=models.CharField(max_length=30, null=True, blank=True)
    building_gas_provider=models.CharField(max_length=30,null=True, blank=True)
    building_water_provider=models.CharField(max_length=30,null=True, blank=True)
    validated_by_admin=models.BooleanField(default=False)
    building_rules_regulations=models.FileField(upload_to=building_directory_path, validators=[FileExtensionValidator(["pdf","doc","docx"],"Files need to be in PDF or Word format")], null=True, blank=True)
    tenant_acknowledgement_form=models.FileField(upload_to=building_directory_path, validators=[FileExtensionValidator(["pdf","doc","docx"],"Files need to be in PDF or Word format")], null=True, blank=True)

    def __str__(self):
        return '{} {} {} {}'.format(
            self.building_name,
            self.address,
            self.city,
            self.postal_code
            )
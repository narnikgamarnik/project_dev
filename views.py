from django.shortcuts import render, get_object_or_404, render_to_response
from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth import authenticate,login
from account.forms import HST_Rebate_form, HST_Rebate_Admin_form, HST_Rebate_payments_form, rebate_estimate_form, BuildingForm, Property_details, New_Property
from django.contrib.auth.models import User
from account.models import HSTRebate_model,HSTRebate_payments_model, User_Profile,hst_pricing, property_access, Buildings, Properties
from django.shortcuts import redirect
from ipware.ip import get_ip
from django.contrib.gis.geoip2 import GeoIP2
from .forms import LoginForm,UserRegistrationForm,EmailPostForm
from django.core.mail import send_mail
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template import loader
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.decorators import login_required
#from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import EmailMessage
from dal import autocomplete
from django.db.models import Q
import operator
from six.moves import reduce


@login_required
def new_property(request):
    if request.method=='POST':
        f=New_Property(request.POST)
        if f.is_valid():
            f2=f.save(commit=False)
            f2.building_id=40
            f2.added_by=request.user
            f2.save()
            access=property_access.objects.create(prop=f2, prop_user=request.user, prop_access_description='Owner')
        return redirect('dashboard')
    else:
        b=Buildings.objects.get(id=40).building_name
        f=New_Property()
        return render(request, 'account/new_property.html',{'b':b, 'f':f})

@login_required
def add_building(request):
    if request.method=='POST':
        f = BuildingForm(request.POST)
        if f.is_valid():
            new_building=f.save(commit=False)
            new_building.added_by=request.user
            f.save()
        return render(request, 'account/new_property.html')
    else:
        f=BuildingForm()
        return render(request, 'account/add_building.html', {'f':f})

@login_required
def property_details(request, prop_id=None):
    prop=Properties.objects.get(id=prop_id)
    if request.method=='POST':
        f= Property_details(request.POST, instance=prop)
        if f.is_valid():
            f.save()
            return redirect('dashboard')
    else:
        prop=Properties.objects.get(id=prop_id)
        f= Property_details(instance=prop)
        return render(request, 'account/property_details.html', {'f':f})

@login_required
def add_lease(request, prop_id=None):
    prop=Properties.objects.get(id=prop_id)
    if request.method=='POST':
        pass
    else:
        return render(request, 'account/add_lease.html')





class BuildingsAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Buildings.objects.none()

        qs = Buildings.objects.all()

        if self.q:
            query_list = self.q.split()
            qs = qs.filter(
                reduce(operator.and_,
                    (Q (building_name__icontains=z) for z in query_list)) | 
                reduce(operator.and_,
                    (Q (address__icontains=z) for z in query_list)) |
                reduce(operator.and_, 
                    (Q (city__icontains=z) for z in query_list)) |
                reduce(operator.and_,
                    (Q (postal_code__icontains=z) for z in query_list))
                )[:10]

        return qs
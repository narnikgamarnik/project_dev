from django import forms
from account.models import HSTRebate_model, HSTRebate_payments_model,User_Profile, Buildings, Properties, property_lease_info
import datetime
from django.forms.extras.widgets import SelectDateWidget
from dal import autocomplete

class Property_details(forms.ModelForm):
    property_type=forms.CharField(max_length=10, required=False)
    status=forms.CharField(max_length=20, required=False)
    purchase_value=forms.CharField(max_length=10, required=False)
    purchase_date=forms.DateField(widget=SelectDateWidget(years=range(1900,2018)), required=False)
    bedrooms=forms.CharField(max_length=3, required=False)
    bathrooms=forms.CharField(max_length=3, required=False)
    size_sqft=forms.CharField(max_length=6, required=False)
    class Meta:
        model=Properties
        fields=('unit_num','property_type','status','purchase_value','purchase_date','bedrooms','bathrooms','size_sqft','search')


class Property_lease(forms.ModelForm):
    class Meta:
        model=property_lease_info
        fields={'lease_date_start', 'lease_date_end', 'tenant_name','tenant_email1', 'tenant_email2', 'tenant_phone1', 'tenant_phone2', 'lease_amount', 'received_all_checks', 'received_checks_note'}


class New_Property(forms.ModelForm):
    class Meta:
        model=Properties
        fields=('unit_num', 'building')
        widgets = {
            'building': autocomplete.ModelSelect2(url='accounts:buildings-autocomplete')
        }

class BuildingForm(forms.ModelForm):
    building_name=forms.CharField(label='', required=True, widget=forms.TextInput(attrs={'placeholder':'Building Name and Tower'}))
    address=forms.CharField(label='', required=True, widget=forms.TextInput(attrs={'placeholder':'Address'}))
    city=forms.CharField(label='', required=True, widget=forms.TextInput(attrs={'placeholder':'City'}))
    postal_code=forms.CharField(label='', required=True, widget=forms.TextInput(attrs={'placeholder':'Postal Code'}))
    class Meta:
        model=Buildings
        fields=('building_name', 'address', 'city', 'postal_code')

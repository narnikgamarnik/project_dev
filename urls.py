from .views import BuildingsAutocomplete
from django.conf.urls import url

urlpatterns = [
    url(
        r'^buildings-autocomplete/$',
        BuildingsAutocomplete.as_view(),
        name='buildings-autocomplete',
    ),
]
